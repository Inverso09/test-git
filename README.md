# ProyectoAlcaldiaNodeJs

Seguir los siguientes comandos con visual studio para correr el proyecto

1) Descargar repositorio
2) cd test-git
3) cd red-bicicletas
4) npm install
5) npm run devstart
6) Abrir navegador y colocar "localhost:3000"

## Hola Bienvenid@ a EXPRESS.JS

## ¿ Qué haremos ?

Proyecto de bicicletas con NodeJs, Express y la plantilla de vistas pug.

## ¿ Qué aprenderemos ?

* Aprenderemos lo que se necesita para comenzar un proyecto con Node JS.
* Qué es express ?
* Conceptos fundamentales: middleware, APIs Asíncronas, rutas.
* Pug (motor de plantillas para la vista).
* Consumo de una API.
* Mostrar los datos consumidos en la UI y estilizarlos.