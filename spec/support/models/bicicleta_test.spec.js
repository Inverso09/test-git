var Bicicleta = require('../../../models/bicicleta');
var mongoose = require('mongoose');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        
        const db = mongoose.connection;
        
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database:');
            console.log("------------------");
            done();
            
        });
    });
    
        afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });
});


describe('Bicicleta.createInstance', () => {
    it('crea una instancia de bicicleta', () => {
        var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

        expect(bici.code).toBe(1);
        expect(bici.color).toBe("verde");
        expect(bici.modelo).toBe("urbana");
        expect(bici.ubicacion[0]).toEqual(-34.5);
        expect(bici.ubicacion[1]).toEqual(-54.1);
    });
});

describe('Bicicleta.allBicis', () => {
    it('comienza vacio', () => {
        Bicicleta.allBicis(function(err, bicis){
            expect(bicis.lenght).toBe(0);
        });
    });
});



describe('Bicicleta.add', () => {
    it('agrega una bicicleta', () => {
       var bici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });

        Bicicleta.add(bici, function(err, newBici){
            if(err) console.log(err);
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.lenght).toEqual(1);
                expect(bicis[0].code).toEqual(bici.code);


            });
        });

    });
});



describe('Bicicleta.findByCode', () =>{
    it('devolver la bicicleta con el codigo buscado', () => {
        Bicicleta.allBicis(function(err, bicis){
            expect(bicis.lenght).toBe(0);

            var bici = new Bicicleta( { code: 1, color: "verde", modelo: "urbana" });

            Bicicleta.add(bici, function(err, newBici){
                if(err) console.log(err);

            var bici2 = new Bicicleta( { code: 2, color: "roja", modelo: "montania" });

            Bicicleta.add(bici2, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.findByCode(1, function(error, targetBici){
                    expect(targetBici.code).toBe(bici.code);
                    expect(targetBici.color).toBe(bici.color);
                    expect(targetBici.modelo).toBe(bici.modelo);

                });
            });

            });

        });
    });
});


/*
beforeEach(() => { Bicicleta.allBicis = []; });
beforeEach(() => { console.log('testeando...'); });


    describe('Bicicleta.allBicis', () =>{
        it('Comienza vacia', () =>{
            expect(Bicicleta.allBicis.length).toBe(0);
        });
    });




describe('Bicicleta.add', () =>{
    it('agregamos una bicicleta', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta( 1, 'rojo', 'urbana', [3.4725994, -76.4909981]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });
});



describe('bicicleta.findById', () =>{
    it('Devolver bicicleta con id 1', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta( 1, 'rojo', 'urbana', [3.4725994, -76.4909981]);
        var b = new Bicicleta( 2, 'azul', 'montaña', [3.4725994, -76.4909981]);
        Bicicleta.add(a);
        Bicicleta.add(b);
        var bici = Bicicleta.findById(1);
        expect(bici.id).toBe(1);
        expect(bici.color).toBe(a.color);
        expect(bici.modelo).toBe(a.modelo);
     });
});


describe('Bicicleta.delete', () =>{
    it('Eliminamos una bicicleta', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta( 1, 'rojo', 'urbana', [3.4725994, -76.4909981]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        var bici = Bicicleta.findById(1);
        Bicicleta.remove(bici.id);
        expect(Bicicleta.allBicis.length).toBe(0);

    });
});
*/