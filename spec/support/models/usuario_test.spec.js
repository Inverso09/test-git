var Bicicleta = require('../../../models/bicicleta');
var Usuario = require('../../../models/usuario');
var Reserva = require('../../../models/reserva');
var mongoose = require('mongoose');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        
        const db = mongoose.connection;
        
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database:');
            console.log("------------------");
            done();
            
        });
    });
    
        afterEach(function(done){
            Reserva.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Usuario.deleteMany({}, function(err, success){
                    if (err) console.log(err);
                    Bicicleta.deleteMany({}, function(err, success){
                        if (err) console.log(err);
                        done();
                    });
                });
            });
        });
});


describe('cuando un usuario ', () =>{
    it('debe existir la reserva', () =>{
        const usuario = new Usuario({nombre: "Felipe"});
        usuario.save();
        const bicibleta = new Bicicleta({code: 1, color:"roja", modelo:"urbana"});
        bicibleta.save();

        var hoy = new Date();
        var mañana = new Date();
        mañana.setDate(hoy.getDate() + 1 );
        usuario.reservar(bicibleta.id, hoy, mañana, function(err, reserva){
            Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                console.log(reservas[0]);
                console.log(reservas[0].usuario.nombre);
                expect(reservas.length).toBe(1);
                expect(reservas[0].diasDeReserva()).toBe(2);
                expect(reservas[0].bicibleta.code).toBe(1);
                expect(reservas[0].usuario.nombre).toBe(usuario.nombre);

            });
        });
    });
});




