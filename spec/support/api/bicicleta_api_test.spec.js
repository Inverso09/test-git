var mongoose = require('mongoose');
var Bicicleta = require('../../../models/bicicleta');
var request = require('request');
var server = require('../../../bin/www');

var base_url = "http://localhost:3000/api/bicicletas"

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        
        const db = mongoose.connection;
        
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database:');
            console.log("------------------");
            done();
            
        });
    });
    
        afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });
}); 


    /*describe('GET BICICLETAS /', () =>{
        it('Status 200', () =>{
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
            });
        });
    });*/

describe('POST BICICLETA /create', () => {
    it('Status 200', () => {
        var headers = {'content-type' : 'application/json'};
        var aBici = '{ "code": 10, "color":"rojo", "modelo": "urbana", "lat": -34, "lng": -31}';

        console.log(aBici);
        request.post({
            headers: headers,
            url: base_url + '/create',
            body: aBici
        }, function(error, response, body){
            expect(response.statusCode).toBe(200);
            var bici = JSON.parse(body).bicicleta;
            console.log(bici);
            expect(bici.color).toBe("rojo");
            expect(bici.ubicacion[0]).toBe(-34);
            expect(bici.ubicacion[1]).toBe(-31);
        });
    });
});

/*describe('Bicicleta API', () =>{
    describe('GET BICICLETAS /', () =>{
        it('Status 200', () =>{
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta( 1, 'rojo', 'urbana', [3.4725994, -76.4909981]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).ToBe(200);

            });

        });

    });
});*/




/*describe('POST BICICLETAS /create', () => {
    it('STATUS 200', () => {
        var headers = {'content-type': 'application/json'};
        var aBici = '{ "id": 10, "color":"rojo", "modelo": "urbana", "lat": "-34", "lng": "-31"}';

        request.post({
            headers: headers,
            url: "http://localhost:3000/api/bicicletas/create",
            body: aBici
        }, function(error, response, body) {
            expect(Bicicleta.findById(10).color).ToBe("rojo");
            
        });
     });
    });*/




    /*describe('DELETE BICICLETAS /delete', () => {
        it('STATUS 204', (done) => {
            var headers = {'content-type': 'application/json'};
            var aBici = '{ "id": 10, "color":"rojo", "modelo": "urbana", "lat": "-34", "lng": "-31"}';
    
            request.delete({
                headers: headers,
                url: "http://localhost:3000/api/bicicletas/delete",
                body: aBici
            }, function(error, response, body) {
                Bicicleta.remove(aBici.id);
                expect(Bicicleta.allBicis.length).toBe(0);
                done();
    
            });
         });
        });*/