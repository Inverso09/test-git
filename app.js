require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usuarioRouter = require('./routes/usuarios');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuarioAPIRouter = require('./routes/api/usuarios');
var tokenRouter = require('./routes/token');
var passport = require('./config/passport');
var session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);
var jwt = require('jsonwebtoken');
var authAPIRouter = require('./routes/api/auth');



//const store = new session.MemoryStore;

var app = express();

let store;

if (process.env.NODE_ENV === 'development') {
  store = session.MemoryStore();

} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });

  store.on('error', function (err) {
    assert.ifError(err);
    assert.ok(false);
  });
}

app.set('secretKey', 'inverso09');
app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis'
}));


//configuracion de mongodb
var mongoose = require('mongoose');
//Si estoy en el ambiente de desarrollo usar mongoDB localhost, es la recomendacion
//var mongoDB = 'mongodb://localhost/red_bicicletas';
//sino usar el mongoatlas, es la recomendacion
//mongodb+srv://admin:<password>@cluster0.diqei.mongodb.net/<dbname>?retryWrites=true&w=majority
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, { useNewUrlParser : true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.session());


const Usuario = require('./models/usuario');
const token = require('./models/token');


app.get('/privacy_policy', function (req, res) {  
  res.sendFile('public/privacy_policy.html');
});

app.get('/google694a47787ed26fa0.html ', function (req, res) {  
  res.sendFile('public/google694a47787ed26fa0.html ');
});
 

app.get('/login', function (req, res) {  
  res.render('sessions/login');
});

app.post('/login', function(req, res, next){
  passport.authenticate('local', function(err, usuario, info){
    if(err) return next(err);
    if(!usuario) return res.render('sessions/login', {info});
    req.login(usuario, function(err){
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);

});

app.get('/logout', function(req, res){
  req.logout();
  console.log("saliendo de la app");
  res.redirect('/');
});

app.get('/forgotPassword', function(req, res){
  res.render('sessions/forgotPassword');
});

app.post('/forgotPassword', function(req, res){
  Usuario.findOne({ email: req.body.email }, function(err, usuario) {
    if (!usuario) return res.render('sessions/forgotPassword', { info: { message: 'No existe el email para un usuario existente' } });
    
    usuario.resetPassword(function(err) {
      if (err) return next(err);
      console.log('sessions/forgotPasswordMessage');
    });    
    res.render('sessions/forgotPasswordMessage');
  });
});

app.get('/resetPassword/:token', function(req, res, next) {
  console.log(req.params.token);
  token.findOne({ token: req.params.token }, function(err, token) {
    if(!token) return res.status(400).send({ msg: 'No existe un usuario asociado al token, verifique que su token no haya expirado' });
    Usuario.findById(token._userId, function(err, usuario) {
      if(!usuario) return res.status(400).send({ msg: 'No existe un usuario asociado al token.' });
      res.render('sessions/resetPassword', {errors: { }, usuario: usuario});
    });
  });
});


app.post('/resetPassword', function(req, res) {
  if(req.body.password != req.body.confirm_password) {
    res.render('sessions/resetPassword', {errors: {confirm_password: {message: 'No coincide con el password ingresado'}}, usuario: new Usuario({email: req.body.email})});
    return;
  }
  Usuario.findOne({email: req.body.email}, function(err, usuario) {
    usuario.password = req.body.password;
    usuario.save(function(err) {
      if(err) {
        res.render('sessions/resetPassword', {errors: err.errors, usuario: new Usuario({email: req.body.email})});
      } else {
        res.redirect('/login');
      }
    });
  });
});

function loggedIn(req, res, next) {
  if(req.user) {
    next();
  } else {
    console.log('User sin loguearse');
    res.redirect('/login');
  }
};

app.use('/', indexRouter);
app.use('/usuarios', usuarioRouter);
app.use('/token', tokenRouter);

app.use('/bicicletas', loggedIn, bicicletasRouter);

app.use('/api/auth', authAPIRouter);
app.use('/api/bicicletas' , validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios', usuarioAPIRouter);


function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
    if (err) {
      console.log('Error en validar Usuario');
      res.json({ status: "error", message: err.message, data: null });
    } else {
      console.log('Pasó el usuario: ' + req.body.userId);
      req.body.userId = decoded.id;
      console.log('JWT verify: ' + decoded);
      next();
    }
  });
};


/*app.get('/auth/google',
  passport.authenticate('google', {
    scope: [
      'https://www.googleapis.com/auth/plus.login',
      'https://www.googleapis.com/auth/plus.profile.emails.read',
      'profile',
      'email'
    ] 
  })
);

app.get('/auth/google/callback',
  passport.authenticate('google', {
    successRedirect: '/',
    failureRedirect: '/error'
  })
);*/




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
