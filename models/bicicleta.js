var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//esquema de mongo
var bicicletaSchema = new Schema({
    code : Number,
    color : String,
    modelo : String,
    ubicacion : {
        type: [Number], index: { type: '2dsphere', sparse: true }
    },
});



//tostring usando el esquema de mongo
bicicletaSchema.methods.toString = function () {
    return 'code' + this.code + " | color: " + this.color; 
};

bicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb); 
};

bicicletaSchema.statics.createInstance = function( code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.statics.add = function(bici, cb) {
     this.create(bici, cb);
};

bicicletaSchema.statics.findByCode = function(acode, cb) {
    return this.findOne({code: acode}, cb);
};

bicicletaSchema.statics.removeByCode = function(acode, cb) {
    return this.deleteOne({code: acode}, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);





/*var Bicicleta = function ( id, color, modelo, ubicacion) {
     this.id = id;
     this.color = color;
     this.modelo = modelo;
     this.ubicacion = ubicacion;
}*/

/*Bicicleta.prototype.toString = function () {
    return 'id' + this.id + " | color: " + this.color; 
};*/

/*Bicicleta.allBicis = [];
Bicicleta.add = function (aBici) {
    Bicicleta.allBicis.push(aBici);
};*/

/*Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find( x => x.id == aBiciId);
    if(aBici){
        return aBici;
    }
    else{
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
    }
};*/

/*Bicicleta.remove = function(aBiciId){
    for(var i =0; i<Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
};*/



/*var a = new Bicicleta( 1, 'rojo', 'urbana', [3.4725994, -76.4909981]);
var b = new Bicicleta( 2, 'azul', 'nose', [3.4668157, -76.4863657]);

Bicicleta.add(a);
Bicicleta.add(b);
*/

