var mymap = L.map('main_map').setView([3.4725994, -76.4909981], 13);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);




$.ajax({
    method: 'POST',
    dataType: 'json',
    url: 'api/auth/authenticate',
    data: { email: 'prueba@gmail.com', password: 'hola12345' },
}).done(function( data ) {
    $.ajax({
        dataType: 'json',
        url: 'api/bicicletas',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("x-access-token", data.data.token);
        }
    }).done(function (result) {
        result.bicicletas.forEach(bicicleta => {
            L.marker(bicicleta.ubicacion, { title: bicicleta.id }).addTo(map);
        });
    });
});