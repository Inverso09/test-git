var Bicicleta = require('../../models/bicicleta');


exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function (error, result) {
        res.status(200).json({
            bicicletas: result
        });  
    });
}

exports.bicicleta_create = function(req, res) {
    let bicicleta = new Bicycle({
        code: req.body.id, 
        color: req.body.color, 
        modelo: req.body.model,
        ubicacion: [req.body.lat, req.body.lng]
    });

    Bicicleta.add(bicicleta, function (error, newElement) { 
        res.status(200).json({
            bicicleta: newElement
        });
    })
};

/*exports.bicicleta_update = function(req, res) {
    var bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    
    res.status(200).json({
        bicicleta: bici
    });
};*/




exports.bicicleta_delete= function (req, res){
    Bicicleta.removeByCode(req.body.code, function (err) {
        res.status(204).send();
    });
};

