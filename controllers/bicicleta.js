var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res) {
    Bicicleta.allBicis(function (error, result) {
        res.render('bicicletas/index', {bicicletas: result});
    });
};

exports.bicicleta_create_get = function(req, res) {
    res.render('bicicletas/create');
};

exports.bicicleta_create_post = function(req, res) {
    let bicicleta = new Bicicleta({
        code: req.body.id, 
        color: req.body.color, 
        modelo: req.body.model,
        ubicacion: [req.body.lat, req.body.lng]
    });

    Bicicleta.add(bicicleta, function (error, newElement) { 
        res.redirect('/bicicletas');
    });
};

exports.bicicleta_remove_post= function (req, res){
    Bicicleta.removeByCode(req.body.code, function (err) {
        res.redirect('/bicicletas');
    });
};



exports.bicicleta_update_get = function(req, res) {
    Bicicleta.findByCode(req.params.code, function(err, bicicleta){
        res.render('bicicletas/update', {bicicleta});
    }); 
};

exports.bicicleta_update_post = function(req, res) {
    Bicicleta.findByCode(req.params.code, function (err, bicicleta) {
        bicicleta.color = req.body.color;
        bicicleta.modelo = req.body.model;
        bicicleta.ubicacion = [req.body.lat, req.body.lng];
        bicicleta.save();
        
        res.redirect('/bicicletas');
    });
};